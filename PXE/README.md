# Preparation d'un RPI sous Fedora 35 pour faire un serveur PXE pour deployer/provisionner vos serveurs (preseed/cloudinit/ansible)

## Rationale :

* Nous voulons installer des systemes GNU/Linux sur notre infrastructure/parc.
* Nous avons besoin d'un serveur DHCP/PXE qui prend en charge l'UEFI.
* Nous souhaitons une faible consommation electrique.
* Nous voulons pouvoir deployer nos configurations manager nos vm/containers via ce même serveur.

__REMARQUE__ : Ce howto est destiné à une installation sur RPI mais fonctionne tres bien sur un fedora server en VM ou sur une autre architecture.

## Prerequis : 

* Un rpi 2 (pas testé avec d'autres)
* Une microSD d'au moins 8 GiB
* Un systeme GNU/Linux pour effectuer toutes les operations.

## Sommaire :

1. On recupere les elements necessaires. 
2. On ecrit le systeme sur la SD.
3. On configure le reseau et ssh.
4. On fait une image de la SD.
5. On boot et on verifie la bonne connectivité
6. On deploie le serveur pxe via ansible

### 1. On recupere les elements necessaires.

On DL l'image de Fedora Minimal qui correspond à notre RPI et on **verifie le checksum**


```bash
wget -c https://download.fedoraproject.org/pub/fedora/linux/releases/35/Spins/armhfp/images/Fedora-Minimal-35-1.2.armhfp.raw.xz
curl -s https://getfedora.org/static/checksums/35/images/Fedora-Spins-35-1.2-armhfp-CHECKSUM | grep '(Fedora-Minimal-35-1.2.armhfp.raw.xz)' | awk '{print $NF}'
sha256sum Fedora-Minimal-35-1.2.armhfp.raw.xz
```

Si on veut utiliser arm-image-installer. Il s'agit d'un script bash qui permet d'installer Fedora sur la SD avec quelques options bien interessantes.

a. on l'installe sur une distrib fedora 

```bash
dnf install -y arm-image-installer
```
b. Sur une autre distro on recupere les sources: 

```bash
git clone https://pagure.io/arm-image-installer.git
```

### 2. On ecrit le systeme sur la SD.


#### A. Methode arm-image-installer

Pas envie/possible de brancher le rpi à un ecran et un clavier pour le post conf en graphique.
On installe fedora 35 sur le rpi avec cette commande l'outil arp-image-installer

```bash
sudo ./arm-image-installer --image=Fedora-Minimal-35-1.2.armhfp.raw.xz --target=rpi2 --media=/dev/mmcblk0 --norootpass --showboot --addkey=rpi.pub --resizefs
```
Si vous avez des erreurs consultez le troubleshooting.


#### B. Methode à la mano 

* Cette methode implique que vous devrez mettre la SD sans le rpi et le booter avec clavier/ecran afin de finaliser la conf.

```bash
xzcat Sources/Fedora-Server-35-1.2.armhfp.raw.xz | sudo dd status=progress bs=4M of=/dev/mmcblk0 && sync
```


### 3. On configure le reseau et ssh.

Pour le reseau nous editons directement la conf sur la SD :

```bash
mount /dev/mmcblk0p3 /mnt/
```

```bash
tee <<EOF /mnt/etc/sysconfig/network-scripts/ifcfg-eth0 >/dev/null
# Static IP
DEVICE=eth0
BOOTPROTO=static
IPADDR=192.168.6.254
NETMASK=255.255.255.0
GATEWAY=192.168.6.1
DNS1=9.9.9.9
DNS2=1.1.1.1
IPV6INIT=no
ONBOOT=yes
TYPE=Ethernet
EOF
```
Idem pour ssh : 

```bash
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /mnt/etc/ssh/sshd_config
```


On peut demonter notre SD.

```bash
umount -R /mnt
``` 

### 4. On lance le rpi et on fini les reglages.


On setup Fedora Minimal : 

[OPTIONNEL] Si le reseau n'a pas fonctionné on peut utiliser NetworkManager en cli : 

```bash
nmcli con add con-name eth0 type ethernet ifname eth0 ipv4.method manual ipv4.address 192.168.6.250/24 ipv4.gateway 192.168.6.1 ipv4.dns 9.9.9.9 ipv6.method disabled
```


On vire les services du pare-feu qui ne nous interessent pas et on ajoute les regles qui vont bien :
```bash
firewall-cmd --target=default --permanent --remove-service=dhcpv6-client
firewall-cmd --target=default --permanent --remove-service=cockpit
firewall-cmd --target=default --permanent --remove-service=mdns
firewall-cmd --zone=FedoraServer --permanent --add-service=ssh
firewall-cmd --zone=FedoraServer --permanent --add-service=tftp
firewall-cmd --zone=FedoraServer --permanent --add-service=dhcp
firewall-cmd --zone=FedoraServer --permanent --add-service=nfs
firewall-cmd --complete-reload
firewall-cmd --list-all
```
Si on veut pouvoir logguer les paquets bloqués sur notre pare-feu rien de plus simple ! 

```bash
firewall-cmd --set-log-denied all
firewall-cmd --complete-reload
journalctl -k -f -g reject
```

Un autre detail du pare-feu. Ne pas laisser un service changer les regles par lui-même : 

```bash
firewall-cmd --query-lockdown
firewall-cmd --lockdown-on
firewall-cmd --complete-reload
```

#### On adapte notre localité.

```shell
timedatectl set-timezone Europe/Paris
localectl set-locale LANG=en_US
localectl set-keymap fr-azerty
```

Cool le systeme est en place.

On peut eteindre le rpi afin de faire une image de la SD.

### 4. On fait une image de la SD avant d'agrandir le logical volume LV et le FS xfs (par defaut)

Une fois **setup correctement** on va faire un backup de notre SD juste au cas ou un truc cloche par la suite.

à partir du laptop: 

```bash
dd if=/dev/mmcblk0 conv=notrunc,sync status=progress | lz4 -9 > rpixe_net_ssh_ok.tar.xz
```
Pour une SD de 16Gio toute fraiche cela donne : 

`1,8G    rpixe_ssh_1.66.tar.xz`

### 5. On boot le rpi.

On met la SD dans le rpi et on le branche.

Si tout va bien on redimensionne notre / : 

```bash
lvresize -l +100%FREE /dev/fedora_fedora/root
xfs_growfs -d /
```

### 6. On deploie le serveur pxe manuellement ou ansible


1. Manuellement: 

```bash
dnf install -y ansible curl dhcp-server nfs-utils syslinux-tftpboot tftp-server tmux vim wget
```

On va configurer nos services : 

`/etc/dhcp/dhcpd.conf`


```shell
option arch code 93 = unsigned integer 16;
if option arch = 00:07 {
	next-server 192.168.6.254;
filename "debian-installer/amd64/grubx64.efi";
} else {
	next-server 192.168.6.254;
filename "debian-installer/pxelinux.0";
}
subnet 192.168.6.0 netmask 255.255.255.0 {
authoritative;
range 192.168.6.50 192.168.6.200;
default-lease-time 600;
max-lease-time 7200;
option domain-name-servers 9.9.9.9;
option routers 192.168.6.1;
ddns-update-style none;
host curiosity {
hardware ethernet xx:xx:xx:xx:xx:xx;
fixed-address 192.168.6.30;
option host-name "curiosity";
option domain-name "planet.mars";
}
}
```

On ajoute du verbeux `-vvv` à tftp pour mieux comprendre les eventuels problemes : 

`/usr/lib/systemd/system/tftp.service`

```shell
[Unit]
Description=Tftp Server
Requires=tftp.socket
Documentation=man:in.tftpd

[Service]
ExecStart=/usr/sbin/in.tftpd -s -vvv /var/lib/tftpboot
StandardInput=socket

[Install]
Also=tftp.socket
```
On configure le nfs pour faire du tcp sur le 2049 en version 4.2: 

`/etc/nfs.conf`

```shell
[nfsd]
debug=4
# threads=8
# # host=
port=2049
# # grace-time=90
# # lease-time=90
udp=n
tcp=y
# # vers2=n
# # vers3=y
# # vers4=y
# # vers4.0=y
# # vers4.1=y
vers4.2=y
# # rdma=n
# #
#
```

On configure la racine du serveur nfs : 

`/etc/exports`

```shell
/srv/nfs/ 192.168.6.0/24(rw,no_root_squash)
```

On verifie :

```bash
exportfs -rav
```

On enable et on demarre nos services en même temps : 

```bash
systemctl enable --now dhcp
systemctl enable --now tftp
systemctl enable --now nfs-server
```
On peut verifier que les ports sont accessibles : 

```bash
ss -lapunte | grep -E 'tftp|dhcp|sshd|nfs'
```

### Debian 11 pxe UEFI:

On dl l'image netboot de debian 11 et on la met au bon endroit :

```bash
wget -c http://ftp.nl.debian.org/debian/dists/bullseye/main/installer-amd64/current/images/netboot/netboot.tar.gz -P /var/lib/tftpboot/
tar xvf /var/lib/tftpboot/netboot.tar.gz -C /var/lib/tftpboot/
```

On met les permissions correctes : 

```bash
chmod -R g=rwx,o=rx /var/lib/tftpboot
```

On voit que nous avons les dossiers/fichiers necessaires pour Debian 11:

```shell
total 37M
drwxrwxr-x.  7 root root 4,0K  2 déc.  19:23 .
drwxr-xr-x. 47 root root 4,0K 29 nov.  19:52 ..
drwxr-xr-x.  2 root root  151 30 nov.  20:00 autoinstall
drwxrwxr-x.  3 root root   19  4 oct.  10:07 debian-installer
drwxr-xr-x.  2 root root    6  2 déc.  19:23 f35s
drwxr-xr-x.  2 root root   39  2 déc.  19:23 f35w
-rwxr-xr-x.  1 root root  12K 29 nov.  21:17 grub.example
lrwxrwxrwx.  1 root root   47  4 oct.  10:07 ldlinux.c32 -> debian-installer/amd64/boot-screens/ldlinux.c32
-rw-rwxr-x.  1 root root  37M  4 oct.  10:07 netboot.tar.gz
lrwxrwxrwx.  1 root root   33  4 oct.  10:07 pxelinux.0 -> debian-installer/amd64/pxelinux.0
lrwxrwxrwx.  1 root root   35  4 oct.  10:07 pxelinux.cfg -> debian-installer/amd64/pxelinux.cfg
drwxrwxr-x.  2 root root   35 29 nov.  21:05 ubunuts
-rw-rwxr-x.  1 root root   65  4 oct.  10:07 version.info

```

### Ubuntu 20.04 UEFI:

On se procure l'iso :

```bash
wget -c http://www.releases.ubuntu.com/20.04/ubuntu-20.04.3-desktop-amd64.iso -P /srv/nfs/isos/
curl http://www.releases.ubuntu.com/20.04/SHA256SUMS
sha256sum /srv/nfs/isos/ubuntu-20.04.3-desktop-amd64.iso
```

On monte l'iso et on prend les fichiers essentiels pour le boot via tftp : 

```bash
mount /srv/nfs/isos/ubuntu-20.04.3-desktop-amd64.iso /mnt/loop/
cp /mnt/loop/casper/{vmlinuz,initrd} /var/lib/tftpboot/ubunuts/
```

On copie toute l'iso sur le serveur nfs:

```bash
rsync -zav /mnt/loop/ /srv/nfs/ubunuts/
```
Ok les fichiers sont en place.

### Fedora 35 UEFI: 

Pour obternir la liste des depots fedora 35 : 

https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-35&arch=x86_64

Ok on choisi un depot, on dl et extrait ce qui nous interesse (les shim et grub ): 

```bash
mkdir sources && cd sources
wget -c https://ftp.acc.umu.se/mirror/fedora/linux/development/35/Everything/x86_64/os/Packages/s/shim-x64-15.4-5.x86_64.rpm
wget -c https://ftp.acc.umu.se/mirror/fedora/linux/development/35/Everything/x86_64/os/Packages/g/grub2-efi-x64-2.06-6.fc35.x86_64.rpm
rpm2cpio grub2-efi-x64-2.06-6.fc35.x86_64.rpm | cpio -idmv
rpm2cpio shim-x64-15.4-5.x86_64.rpm | cpio -idmv
cp ./boot/efi/EFI/fedora/{shimx64.efi,grubx64.efi} /var/lib/tftpboot/f35/
```

On va mettre en place le menu grub pour nos systemes : 

`/var/lib/tftpboot/debian-installer/amd64/grub/grub.cfg`

```shell
if loadfont $prefix/font.pf2 ; then
  set gfxmode=800x600
  set gfxpayload=keep
  insmod efi_gop
  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod gfxterm
  insmod png
  terminal_output gfxterm
fi

if background_image /isolinux/splash.png; then
  set color_normal=light-gray/black
  set color_highlight=white/black
elif background_image /splash.png; then
  set color_normal=light-gray/black
  set color_highlight=white/black
else
  set menu_color_normal=cyan/blue
  set menu_color_highlight=white/blue
fi

insmod play
play 960 440 1 0 4 440 1
set default="d11luksauto"
set timeout=3

menuentry "Ubuntu 20.04.3 Fully Automated UEFI BTRFS NOLUKS"  --class fedora --class gnu-linux --class gnu --class os --id ubunutsautoluks {
        set background_color=black
        linuxefi    ubunuts/vmlinuz root=/dev/nfs boot=casper netboot=nfs nfsroot=192.168.6.254:/srv/nfs/ubunuts net.ifnames=0 biosdevname=0 ipv6.disable=1 language=fr locale=fr_FR.UTF-8 keymap=fr keyboard-configuration/layoutcode=fr ip=dhcp rw debian-installer/allow_unauthenticated_ssl=true automatic-ubiquity url=tftp://192.168.6.254/autoinstall/ubiseed20_ok.cfg netcfg/get_hostname=install netcfg/get_domain=install vga=788 noprompt DEBCONF_DEBUG=5 --- quiet
        initrdefi   ubunuts/initrd
}

menuentry 'Ubuntu 20.04.3 Manual via NFS' --id ubunutsmanual {
	linuxefi ubunuts/vmlinuz root=/dev/nfs boot=casper netboot=nfs nfsroot=192.168.6.254:/srv/nfs/ubunuts locale=fr_FR.UTF-8 net.ifnames=0 biosdevname=0 ipv6.disable=1 keyboard-configuration/layoutcode=fr ip=dhcp rw hostname=curiosity domain=planet.mars automatic-ubiquity url=http://192.168.6.254/autoinstall/ubiseed_20_ssd.cfg debian-installer/allow_unauthenticated_ssl=true DEBCONF_DEBUG=5
	initrdefi ubunuts/initrd
}

menuentry "Debian 11 Fully Automated UEFI LVM LUKS BTRFS Remote Decrypt DEBUG TTY4" --id d11luksauto {
        set background_color=black
        linuxefi    /debian-installer/amd64/linux auto=true url=tftp://192.168.6.254/preseed/debseed11_crypt.cfg netcfg/get_hostname=install netcfg/get_domain=install net.ifnames=0 biosdevname=0 ipv6.disable=1 language=en locale=en_US.UTF-8 keymap=fr vga=788 noprompt DEBCONF_DEBUG=5 --- quiet
        initrdefi   /debian-installer/amd64/initrd.gz
}

menuentry 'Install' {
    set background_color=black
    linux    /debian-installer/amd64/linux vga=788 --- quiet
    initrd   /debian-installer/amd64/initrd.gz
}
submenu --hotkey=a 'Advanced options ...' {
    set menu_color_normal=cyan/blue
    set menu_color_highlight=white/blue
    set gfxpayload=keep
    menuentry '... Expert install' {
        set background_color=black
        linux    /debian-installer/amd64/linux priority=low vga=788 ---
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry '... Rescue mode' {
        set background_color=black
        linux    /debian-installer/amd64/linux vga=788 rescue/enable=true --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry '... Automated install' {
        set background_color=black
        linux    /debian-installer/amd64/linux auto=true priority=critical vga=788 --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry --hotkey=x '... Expert install with speech synthesis' {
        set background_color=black
        linux    /debian-installer/amd64/linux priority=low vga=788 speakup.synth=soft ---
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry --hotkey=r '... Rescue mode with speech synthesis' {
        set background_color=black
        linux    /debian-installer/amd64/linux vga=788 rescue/enable=true speakup.synth=soft --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry --hotkey=a '... Automated install with speech synthesis' {
        set background_color=black
        linux    /debian-installer/amd64/linux auto=true priority=critical vga=788 speakup.synth=soft --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    submenu '... Desktop environment menu ...' {
        set menu_color_normal=cyan/blue
        set menu_color_highlight=white/blue
        set gfxpayload=keep
        submenu '... GNOME desktop boot menu ...' {
            set menu_color_normal=cyan/blue
            set menu_color_highlight=white/blue
            set gfxpayload=keep
            menuentry '... Install' {
                set background_color=black
                linux    /debian-installer/amd64/linux desktop=gnome vga=788 --- quiet
                initrd   /debian-installer/amd64/initrd.gz
            }
            submenu '... GNOME advanced options ...' {
                set menu_color_normal=cyan/blue
                set menu_color_highlight=white/blue
                set gfxpayload=keep
                menuentry '... Expert install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=gnome priority=low vga=788 ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry '... Automated install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=gnome auto=true priority=critical vga=788 --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=x '... Expert install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=gnome priority=low vga=788 speakup.synth=soft ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=a '... Automated install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=gnome auto=true priority=critical vga=788 speakup.synth=soft --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
            }
        }
        submenu '... KDE Plasma desktop boot menu ...' {
            set menu_color_normal=cyan/blue
            set menu_color_highlight=white/blue
            set gfxpayload=keep
            menuentry '... Install' {
                set background_color=black
                linux    /debian-installer/amd64/linux desktop=kde vga=788 --- quiet
                initrd   /debian-installer/amd64/initrd.gz
            }
            submenu '... KDE Plasma advanced options ...' {
                set menu_color_normal=cyan/blue
                set menu_color_highlight=white/blue
                set gfxpayload=keep
                menuentry '... Expert install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=kde priority=low vga=788 ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry '... Automated install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=kde auto=true priority=critical vga=788 --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=x '... Expert install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=kde priority=low vga=788 speakup.synth=soft ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=a '... Automated install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=kde auto=true priority=critical vga=788 speakup.synth=soft --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
            }
        }
        submenu '... LXDE desktop boot menu ...' {
            set menu_color_normal=cyan/blue
            set menu_color_highlight=white/blue
            set gfxpayload=keep
            menuentry '... Install' {
                set background_color=black
                linux    /debian-installer/amd64/linux desktop=lxde vga=788 --- quiet
                initrd   /debian-installer/amd64/initrd.gz
            }
            submenu '... LXDE advanced options ...' {
                set menu_color_normal=cyan/blue
                set menu_color_highlight=white/blue
                set gfxpayload=keep
                menuentry '... Expert install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=lxde priority=low vga=788 ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry '... Automated install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=lxde auto=true priority=critical vga=788 --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=x '... Expert install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=lxde priority=low vga=788 speakup.synth=soft ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=a '... Automated install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=lxde auto=true priority=critical vga=788 speakup.synth=soft --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
            }
        }
    }
}
submenu --hotkey=d 'Dark theme option...' {
    set menu_color_normal=white/black
    set menu_color_highlight=yellow/black
    set color_normal=white/black
    set color_highlight=yellow/black
    background_image
    set gfxpayload=keep
    menuentry '... Install' {
        set background_color=black
        linux    /debian-installer/amd64/linux vga=788 theme=dark --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    submenu --hotkey=a '... Advanced options ...' {
        set menu_color_normal=white/black
        set menu_color_highlight=yellow/black
        set color_normal=white/black
        set color_highlight=yellow/black
        background_image
        set gfxpayload=keep
        menuentry '... Expert install' {
            set background_color=black
            linux    /debian-installer/amd64/linux priority=low vga=788 theme=dark ---
            initrd   /debian-installer/amd64/initrd.gz
        }
        menuentry '... Rescue mode' {
            set background_color=black
            linux    /debian-installer/amd64/linux vga=788 rescue/enable=true theme=dark --- quiet
            initrd   /debian-installer/amd64/initrd.gz
        }
        menuentry '... Automated install' {
            set background_color=black
            linux    /debian-installer/amd64/linux auto=true priority=critical vga=788 theme=dark --- quiet
            initrd   /debian-installer/amd64/initrd.gz
        }
    }
}
menuentry --hotkey=s 'Install with speech synthesis' {
    set background_color=black
    linux    /debian-installer/amd64/linux vga=788 speakup.synth=soft --- quiet
    initrd   /debian-installer/amd64/initrd.gz
}
```

### Les preseed : 

1. Debian 11: 

Un preseed Debian 11 destiné à installer du proxmox derriere:
* installe debian 11 automatiquement en chiffrant (luks1 cypher par default) tout sauf le boot.
* lvm pour aller avec les confs de proxmox
* btrfs parceque j'ai pas de ZRAID c'est un lab singledisk.
* avec des credentials et pubkey.
* une ip fixe.
* la possibilité de l'unlocker à distance via dropbear.

```shell



Lisez le c'est facile à comprendre.


### Troubleshooting 

1. `Error: mount /dev/mmcblk0p2 /tmp/boot failed`

```bash
sgdisk -Z /dev/mmcblk0
partprobe
```
relancez votre commande sans options : 

```bash
sudo ./arm-image-installer -y --image=Fedora-Minimal-35-1.2.armhfp.raw.xz --target=rpi2 --media=/dev/mmcblk0
```

2. l'option `--relabel` semble faire planter le boot dans le rpi.

3. l'option `--resizefs` ne fonctionne pas totalement avec le btrfs : https://pagure.io/arm-image-installer/issue/73

4. networkmanager erreur : 



```shell
TASK [add an ethernet connection with static ip configuration] ******************************************************************************************************************************************************************************
fatal: [192.168.6.73]: FAILED! => {"changed": false, "msg": "Unsupported parameters for (nmcli) module: dnssearch Supported parameters include: ageingtime, arp_interval, arp_ip_target, autoconnect, conn_name, dhcp_client_id, dns4, dns4_search, dns6, dns6_search, downdelay, egress, flags, forwarddelay, gw4, gw6, hairpin, hellotime, ifname, ingress, ip4, ip6, ip_tunnel_dev, ip_tunnel_local, ip_tunnel_remote, mac, master, maxage, miimon, mode, mtu, path_cost, primary, priority, slavepriority, state, stp, type, updelay, vlandev, vlanid, vxlan_id, vxlan_local, vxlan_remote"}

That is because of a wrong module :  dns4_search: not dnssearch ! 
Also watch your interfaces : 

dns change not ok with nmcli module : 
 the -vvv option while launching PB give : 

The full traceback is:                                                                                                                                                                                                                       
Traceback (most recent call last):                                                                                                                                                                                                           
  File "/tmp/ansible_nmcli_payload_hl_zkvk2/ansible_nmcli_payload.zip/ansible/modules/net_tools/nmcli.py", line 568, in <module>                                                                                                             
  File "/usr/lib64/python3.9/site-packages/gi/__init__.py", line 126, in require_version                                                                                                                                                     
    raise ValueError('Namespace %s not available' % namespace)                                                                                                                                                                               
ValueError: Namespace NMClient not available                                                                                                                                                                                                 
fatal: [192.168.6.95]: FAILED! => {
```

Il fallait installer NetworkManager-glib before on the host :

5. TFTP : Voir les logs . 


On a une erreur:  1962 sur notre client : 

Si on augmente le niveau de verbosité de tftp 

`/usr/lib/systemd/system/tftp.service`

On ajoute -vvv comme argument :

`ExecStart=/usr/sbin/in.tftpd -s -vvv /var/lib/tftpboot`


On reload nos units/demons : 

```bash
systemctl daemon-reload
```

On verra les logs plus en detail avec : 


```shell
nov. 27 12:07:34 sojourner in.tftpd[10813]: RRQ from ::ffff:192.168.6.30 filename uefi/f35s/grubx64.efi
nov. 27 12:07:34 sojourner in.tftpd[10813]: sending NAK (0, Permission denied) to ::ffff:192.168.6.30
nov. 27 12:08:38 sojourner in.tftpd[10871]: RRQ from ::ffff:192.168.6.30 filename uefi/f35s/grubx64.efi
nov. 27 12:08:38 sojourner in.tftpd[10871]: sending NAK (0, Permission denied) to ::ffff:192.168.6.30
```

Les permissions ne sont pas bonnes : 

```bash
chmod -R 777 /var/lib/tftpboot
```

Apres cela : 
```shell
nov. 27 12:19:00 sojourner in.tftpd[11298]: RRQ from ::ffff:192.168.6.30 filename uefi/f35s/grub.cfg
nov. 27 12:19:00 sojourner in.tftpd[11298]: Client ::ffff:192.168.6.30 finished uefi/f35s/grub.cfg
```


6. Le nfs ne passe pas et renvoie coté client : 

`NFS over tcp not availible from 192.168.6.254 
read : no route to host`

en journalisant le pare-feu : 

`nov. 29 22:50:15 sojourner kernel: "filter_IN_FedoraServer_REJECT: "IN=eth0 OUT= MAC=xx:xx:xx:xx:xx:yy:yy:yy:yy:yy SRC=192.168.6.59 DST=192.168.6.254 LEN=84 TOS=0x00 PREC=0x00 TTL=64 ID=23740 DF PROTO=UDP SPT=33383 DPT=111 LEN=64`

On voit qu'il tente de passer sur le UDP/111 :

On realise que [pour le nfs il faut ouvrir d'autre ports : ](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9-beta/html/performing_an_advanced_rhel_installation/making-kickstart-files-available-to-the-installation-program_choose-your-title-id#ports-for-network-based-installation_making-kickstart-files-available-to-the-installation-program)

Par ailleurs en faisant : 
```bash
ss -lapunte | grep nfs
```

```shell
tcp   LISTEN 0      4096         0.0.0.0:20048      0.0.0.0:*     users:(("rpc.mountd",pid=9368,fd=4)) ino:58161 sk:9f cgroup:/system.slice/nfs-mountd.service <->                            
tcp   LISTEN 0      4096            [::]:20048         [::]:*     users:(("rpc.mountd",pid=9368,fd=5)) ino:58163 sk:a4 cgroup:/system.slice/nfs-mountd.service v6only:1 <-> 
```

```bash
firewall-cmd --zone=FedoraServer --permanent --add-port=111/tcp
firewall-cmd --zone=FedoraServer --permanent --add-port=111/udp
firewall-cmd --zone=FedoraServer --permanent --add-port=20048/tcp
firewall-cmd --complete-reload
```

Si on veut passer en full v4 voici la procedure : 

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_file_systems/index#configuring-an-nfsv4-only-server_exporting-nfs-shares

### Ressources : 

### RED HAT recommandations PXE :
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/preparing-for-a-network-install_installing-rhel-as-an-experienced-user

### setup network
https://docs.ansible.com/ansible/latest/collections/community/general/nmcli_module.html

### disabling ipv6
https://github.com/juju4/ansible-ipv6/blob/master/tasks/ipv6-disable.yml

### downloading isos
https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html

### mounting isos 
https://docs.ansible.com/ansible/latest/collections/ansible/posix/mount_module.html#ansible-collections-ansible-posix-mount-module

### creating multiple directores with attributes 
https://www.mydailytutorials.com/ansible-create-directory/
https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html 

### setup firewall
https://docs.ansible.com/ansible/latest/collections/ansible/posix/firewalld_module.html#ansible-collections-ansible-posix-firewalld-module



### multiple ports firewalld 
https://www.reddit.com/r/ansible/comments/dfll3y/ansible_firewalld_multiple_ports/

### RPI STUFF
https://docs.fedoraproject.org/en-US/quick-docs/raspberry-pi/

https://esther.codes/post-cryptsetup_raspberry/

https://raspberrytips.com/install-fedora-raspberry-pi/

https://docs.fedoraproject.org/en-US/quick-docs/raspberry-pi/#booting-fedora-on-a-raspberry-pi-for-the-first-time_rpi

https://pagure.io/arm-image-installer/tree/main

### MIXED 

https://gist.github.com/jkullick/9b02c2061fbdf4a6c4e8a78f1312a689

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces
